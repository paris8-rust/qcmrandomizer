# QcmRandomizer

program which create multiple randomized mcq from one.
(changes may come).

# Used libs
- rand
- anyhow
- pdf_canvas
# Objective

- randomize and create as much file as you want from one.
- randomize the question and answers.
- pdf format.
- Name and Date on the top left hand corner.
# Upgrade 

- minimal interface to choose a file.
